--
CREATE DATABASE CarRentalSystem;
--
-- Step 1: Vehicle Table
CREATE TABLE Vehicle (
    vehicleID INT PRIMARY KEY,
    make VARCHAR(255),
    model VARCHAR(255),
    year INT,
    dailyRate DECIMAL(10, 2),
    status VARCHAR(15),
    passengerCapacity INT,
    engineCapacity INT
);

-- Step 2: Customer Table
CREATE TABLE Customer (
    customerID INT PRIMARY KEY,
    firstName VARCHAR(255),
    lastName VARCHAR(255),
    email VARCHAR(255),
    phoneNumber VARCHAR(20)
);

-- Step 3: Lease Table
CREATE TABLE Lease (
    leaseID INT PRIMARY KEY,
    vehicleID INT,
    customerID INT,
    startDate DATE,
    endDate DATE,
    type VARCHAR(15),
    FOREIGN KEY (vehicleID) REFERENCES Vehicle(vehicleID),
    FOREIGN KEY (customerID) REFERENCES Customer(customerID)
);

-- Step 4: Payment Table
CREATE TABLE Payment (
    paymentID INT PRIMARY KEY,
    leaseID INT,
    paymentDate DATE,
    amount DECIMAL(10, 2),
    FOREIGN KEY (leaseID) REFERENCES Lease(leaseID)
);
-- Insert values into Vehicle Table
INSERT INTO Vehicle (vehicleID, make, model, year, dailyRate, status, passengerCapacity, engineCapacity)
VALUES 
(1, 'Mercedes', 'E-Class', 2022, 80.00, 'available', 5, 2000),
(2, 'Toyota', 'Camry', 2021, 60.00, 'available', 5, 2500),
(3, 'Honda', 'Accord', 2022, 65.00, 'notAvailable', 5, 1800);

-- Insert values into Customer Table
INSERT INTO Customer (customerID, firstName, lastName, email, phoneNumber)
VALUES 
(1, 'John', 'Doe', 'john.doe@example.com', '555-1234'),
(2, 'Jane', 'Smith', 'jane.smith@example.com', '555-5678'),
(3, 'Bob', 'Johnson', 'bob.johnson@example.com', '555-9876');

-- Insert values into Lease Table
INSERT INTO Lease (leaseID, vehicleID, customerID, startDate, endDate, type)
VALUES 
(101, 1, 1, '2022-01-01', '2022-01-07', 'DailyLease'),
(102, 2, 2, '2022-02-15', '2022-03-15', 'MonthlyLease'),
(103, 3, 3, '2022-03-01', '2022-03-07', 'DailyLease');

-- Insert values into Payment Table
INSERT INTO Payment (paymentID, leaseID, paymentDate, amount)
VALUES 
(1001, 101, '2022-01-05', 400.00),
(1002, 102, '2022-02-20', 1800.00),
(1003, 103, '2022-03-05', 455.00);
--Q1.Update the daily rate for a Mercedes car to 68:--
UPDATE Vehicle
SET dailyRate = 68
WHERE make = 'Mercedes';
--Q2.Delete a specific customer and all associated leases and payments:
DELETE FROM Customer
WHERE customerID = 2;
--Q3.Rename the "paymentDate" column in the Payment table to "transactionDate":
--ALTER TABLE Payment
--RENAME COLUMN paymentDate TO transactionDate;
--Q4.Find a specific customer by email:
SELECT * FROM Customer
WHERE email = 'john.doe@example.com';
--Q5.Get active leases for a specific customer:
SELECT *
FROM Lease
WHERE customerID = 1
  AND endDate >= GETDATE();
--Q6.Find all payments made by a customer with a specific phone number:
SELECT * FROM Payment
WHERE leaseID IN (
  SELECT leaseID FROM Lease
  WHERE customerID = (
    SELECT customerID FROM Customer
    WHERE phoneNumber = '555-1234'
  )
);
--Q7.Calculate the average daily rate of all available cars:
SELECT AVG(dailyRate) AS averageDailyRate
FROM Vehicle
WHERE status = 'available';
--Q8.Find the car with the highest daily rate:
SELECT TOP 1 *
FROM Vehicle
ORDER BY dailyRate DESC;
--Q9.Retrieve all cars leased by a specific customer:
SELECT * FROM Vehicle
WHERE vehicleID IN (
  SELECT vehicleID FROM Lease
  WHERE customerID = 1
);
--Q10.Find the details of the most recent lease:
SELECT TOP 1 *
FROM Lease
ORDER BY startDate DESC;

--Q11.List all payments made in the year 2023:
SELECT *
FROM Payment
WHERE YEAR(paymentDate) = 2020;
--Q12.Retrieve customers who have not made any payments:
SELECT * FROM Customer
WHERE customerID NOT IN (
  SELECT DISTINCT customerID FROM Payment
);
--Q13.Retrieve Car Details and Their Total Payments:
SELECT
    V.*,
    COALESCE(SUM(P.amount), 0) AS totalPayments
FROM
    Vehicle V
LEFT JOIN
    Lease L ON V.vehicleID = L.vehicleID
LEFT JOIN
    Payment P ON L.leaseID = P.leaseID
GROUP BY
    V.vehicleID, V.make, V.model, V.year, V.dailyRate, V.status, V.passengerCapacity, V.engineCapacity;

--Q14.Calculate Total Payments for Each Customer:
SELECT
    C.*,
    COALESCE(SUM(P.amount), 0) AS totalPayments
FROM
    Customer C
LEFT JOIN
    Lease L ON C.customerID = L.customerID
LEFT JOIN
    Payment P ON L.leaseID = P.leaseID
GROUP BY
    C.customerID, C.firstName, C.lastName, C.email, C.phoneNumber;

--Q15.List Car Details for Each Lease:
SELECT Lease.*, Vehicle.*
FROM Lease
JOIN Vehicle ON Lease.vehicleID = Vehicle.vehicleID;
--Q16.Retrieve Details of Active Leases with Customer and Car Information:
SELECT
    L.*,
    C.*,
    V.*
FROM
    Lease L
JOIN
    Customer C ON L.customerID = C.customerID
JOIN
    Vehicle V ON L.vehicleID = V.vehicleID
WHERE
    L.endDate >= GETDATE();

--Q17.Find the Customer Who Has Spent the Most on Leases:
SELECT TOP 1
    C.*,
    COALESCE(SUM(P.amount), 0) AS totalSpent
FROM
    Customer C
LEFT JOIN
    Lease L ON C.customerID = L.customerID
LEFT JOIN
    Payment P ON L.leaseID = P.leaseID
GROUP BY
    C.customerID, C.firstName, C.lastName, C.email, C.phoneNumber
ORDER BY
    totalSpent DESC;

--Q18.List All Cars with Their Current Lease Information:
SELECT Vehicle.*, Lease.*, Customer.*
FROM Vehicle
LEFT JOIN Lease ON Vehicle.vehicleID = Lease.vehicleID
LEFT JOIN Customer ON Lease.customerID = Customer.customerID;


